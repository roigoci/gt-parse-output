/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.karimunjawa.gapitrans;

import in.karimunjawa.gapitrans.exception.TranslationNotFoundException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pavel
 */
public class GTranslateTest {

    public GTranslateTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testHello() throws TranslationNotFoundException {
        String translate = new GTranslateImpl().translate("Hello", "en", "cs");
        assertEquals(translate, "Ahoj");
    }

    @Test
    public void testFail() {
        try {
            new GTranslateImpl().translate("Hello", "en", "cz");
            assertTrue("Exception was expected", false);
        } catch (TranslationNotFoundException ex) {
            assertTrue("Exception was thrown", true);
        }
    }
    
    @Test
    public void testComplexString() throws TranslationNotFoundException {
        String translate = new GTranslateImpl().translate("He said: \"Hello.\"", "en", "cs");
        assertEquals("Řekl: \\\"Dobrý den.\\\"", translate);
    }

    @Test
    public void testParseMoreSentences() throws TranslationNotFoundException {
        String output = "[[[\"Vstoupil do místnosti. \",\"He entered a room.\",null,null,0],[\"Pak se zeptal mě.\",\"Then he asked me.\",null,null,0]],null,\"en\"]";
        String translate = GTranslateImpl.parseGTOutput(output);
        assertEquals("Vstoupil do místnosti. Pak se zeptal mě.", translate);
    }

    @Test
    public void testTranslateMoreSentences() throws TranslationNotFoundException {
        String translate = new GTranslateImpl().translate("He entered a room. Then he asked me.", "en", "cs");
        assertEquals("Vešel do místnosti. Pak se mě zeptal.", translate);
    }
    
    @Test
    public void testAppostrov() throws TranslationNotFoundException {
        String translate = new GTranslateImpl().translate("It’s good.", "en", "cs");
        assertEquals("Je to dobré.", translate);
    }

    @Test
    public void testComplexSentencWithComma() throws TranslationNotFoundException {
        String output = "[[[\"LLumar okenní fólie pomáhají chránit a zlepšovat domovy, podniky a vozidel po celém světě.\",\"LLumar window films are helping to protect and improve homes, businesses and vehicles around the world.\",null,null,0]],null,\"en\"]";
        String translate = GTranslateImpl.parseGTOutput(output);
        assertEquals("LLumar okenní fólie pomáhají chránit a zlepšovat domovy, podniky a vozidel po celém světě.", translate);
    }
    

}
