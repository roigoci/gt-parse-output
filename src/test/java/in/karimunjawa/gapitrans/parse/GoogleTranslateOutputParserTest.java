/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.karimunjawa.gapitrans.parse;

import in.karimunjawa.gapitrans.parse.GoogleTranslateOutputParser;
import in.karimunjawa.gapitrans.exception.TranslationNotFoundException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pavel
 */
public class GoogleTranslateOutputParserTest {

    public GoogleTranslateOutputParserTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testCommaInString() throws TranslationNotFoundException{
        String gtOut = "[[[\"A, b, c. \",\"A\",null,null,0],"
                + "[\"D, e, f.\",\"B\",null,null,0]],null,\"en\"]";
        String translated = GoogleTranslateOutputParser.parse(gtOut).getTranslatedText();
        assertEquals("A, b, c. D, e, f.", translated);
    }
    
    @Test
    public void testTranslationNotFound() {
        String gtOut = "[[[\"Hello\",\"Hello\",null,null,0]],null,\"en\"]";
        try {
            GoogleTranslateOutputParser.parse(gtOut);
            assertTrue("exception expected", false);
        } catch (TranslationNotFoundException ex) {
        }
    }
    
    @Test
    public void testUnicodeEscape() throws TranslationNotFoundException{
        String gtOut = "[[[\"Eastman Chemical Company je předním světovým výrobcem a prodejcem vysoce výkonných filmů pro automobilový průmysl a architektonické aplikace, a je prodáván pod značkou okenní fólie LLumar® stejně jako pod FormulaOne High Performance Auto Tint® podle LLumar®, Vista ™, Huper Optik \",\"Eastman Chemical Company is the world's leading manufacturer and marketer of high performance films for automotive and architectural applications, and is sold under the LLumar® window film brand as well as under the FormulaOne High Performance Auto Tint® by LLumar®, Vista™, Huper Optik\",null,null,0],[\"\\u0026 amp; \",\"\\u0026amp;\",null,null,0],[\"Design®, EnerLogic®, IQue®, V-KOOL® a Gila® Do-it-yourself okenní fólie značky.\",\"Design®, EnerLogic®, IQue®, V-KOOL® and Gila® Do-it-Yourself window film brands.\",null,null,0]],null,\"en\"]";
        String translated = GoogleTranslateOutputParser.parse(gtOut).getTranslatedText();
        assertEquals("Eastman Chemical Company je předním světovým výrobcem a prodejcem vysoce výkonných filmů pro automobilový průmysl a architektonické aplikace, a je prodáván pod značkou okenní fólie LLumar® stejně jako pod FormulaOne High Performance Auto Tint® podle LLumar®, Vista ™, Huper Optik \\u0026 amp; Design®, EnerLogic®, IQue®, V-KOOL® a Gila® Do-it-yourself okenní fólie značky.", translated);
    }
    
    @Test
    public void testUnknownFail() throws TranslationNotFoundException{
        String gtOut = "[[[\"Instalace a záruka\",\"Installation and warranty\",null,null,0]],null,\"en\"]";
        String translated = GoogleTranslateOutputParser.parse(gtOut).getTranslatedText();
        assertEquals("Instalace a záruka", translated);
    }

}
