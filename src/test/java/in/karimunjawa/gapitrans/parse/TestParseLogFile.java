/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.karimunjawa.gapitrans.parse;

import in.karimunjawa.gapitrans.parse.GoogleTranslateOutputParser;
import in.karimunjawa.gapitrans.TranslateNettePropertyFile;
import in.karimunjawa.gapitrans.parse.GoogleTranslateOutputParser.Output;
import in.karimunjawa.gapitrans.exception.TranslationNotFoundException;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pavel
 */
public class TestParseLogFile {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        BufferedReader br = null;
        String file = "/home/pavel/NetBeansProjects/gt-parse-output/src/main/resources/firstLogOut.log";
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "utf-8"));
            String line = null;
            Map<String, String> vocabulary = new HashMap<>();
            Map<String, String> untranslated = new HashMap<>();
            while((line = br.readLine()) != null) {
                try {
                    String outputText = line.substring("Output: ".length());
                    Output output = GoogleTranslateOutputParser.parse(outputText);
                    System.out.println(outputText);
                    System.out.println(output.getSourceText());
                    System.out.println(output.getTranslatedText());
                    
                    vocabulary.put(output.getSourceText(), output.getTranslatedText());
                } catch (TranslationNotFoundException ex) {
                    // translation not found
                    System.out.println("WARNING: " + line);
                    System.out.println("Translation not found: " + ex.getMessage());
                    untranslated.put(ex.getMessage().trim(), ex.getMessage().trim());
                }
            }
            
            FileOutputStream fos = new FileOutputStream("/home/pavel/NetBeansProjects/gt-parse-output/src/main/resources/vocabulary.ser");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(vocabulary);
            oos.writeObject(untranslated);
            
            System.out.println("Unique keys: " + vocabulary.size());
            System.out.println("Untranslated: " + untranslated.size());
        } catch (IOException ex) {
            Logger.getLogger(TranslateNettePropertyFile.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (br != null) br.close();
            } catch (IOException ex) {
            }
        }
    }
    
}
