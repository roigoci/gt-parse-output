/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.karimunjawa.gapitrans;

import in.karimunjawa.gapitrans.TranslateNettePropertyFile;
import in.karimunjawa.gapitrans.exception.SkipLineException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pavel
 */
public class TranslateNettePropertyFileTest {

    public TranslateNettePropertyFileTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testGetValue() throws SkipLineException {
        String line = "\t\tkey: \"value\"";
        String value = TranslateNettePropertyFile.getValue(line);
        assertEquals("value", value);
    }
    
    @Test
    public void testGetValueForModule() {
        try {
            String line = "module:";
            TranslateNettePropertyFile.getValue(line);
            assertTrue("Exception expected.", false);
        } catch (SkipLineException ex) {
            assertTrue("Expection was thrown correctly",true);
        }
    }

}
