grammar GTOut;

out
    :   '[' translations ',null,' source_language ']'
    ;

translations
    :   '[' translate ( ',' translate)* ']'
    ;

translate
    :   '[' destination ',' source ',null,null,' NUMBER ']'
    ;

destination_language
    :   LANGUAGE_CODE
    ;

source_language
    :   LANGUAGE_CODE
    ;

destination
    : StringLiteral
    ;

source
    : StringLiteral
    ;

NUMBER
    :   ('0'..'9')
    ;

LANGUAGE_CODE
    :   ('"cs"' | '"en"')
    ;

StringLiteral
    :   '"' StringCharacters? '"'
    ;

fragment
StringCharacters
    :   StringCharacter+
    ;
fragment
StringCharacter
    :   ~["\\]
    |   EscapeSequence
    ;

fragment
EscapeSequence
    :   '\\' ["\\]
    |   UnicodeEscape
    ;

fragment
UnicodeEscape
    :   '\\' 'u' HexDigit HexDigit HexDigit HexDigit
    ;

fragment
HexDigit
    :   [0-9a-fA-F]
    ;