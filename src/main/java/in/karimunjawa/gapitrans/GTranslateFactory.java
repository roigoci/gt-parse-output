/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.karimunjawa.gapitrans;

/**
 *
 * @author pavel
 */
public class GTranslateFactory {

    public static GTranslate createInstance() {
        return new GTranslateProxy();
    }

    public static void disposeInstance(GTranslate gTranslate) {
        if (gTranslate instanceof GTranslatePersisted) {
            ((GTranslatePersisted) gTranslate).store();
        }
    }

}
