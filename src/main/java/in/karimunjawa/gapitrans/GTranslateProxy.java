/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.karimunjawa.gapitrans;

import in.karimunjawa.gapitrans.exception.TranslationNotFoundException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pavel
 */
public class GTranslateProxy implements GTranslate, GTranslatePersisted {

    private Map<String, String> vocabulary = new HashMap<>();
    private Map<String, String> untranslated = new HashMap<>();
    private GTranslateImpl impl = new GTranslateImpl();
    private Statistics statistics = new Statistics();

    class Statistics {

        private int vocabulary;
        private int gapi;
        private int untranslated;

        public void vocabularyUsed() {
            vocabulary++;
        }

        public void gapiUsed() {
            gapi++;
        }

        public void untranslatedUsed() {
            untranslated++;
        }

        public String getResult() {
            return "Gapi used #" + gapi + " and cache used #" + vocabulary + " and untranslated #" + untranslated;
        }
    }

    public GTranslateProxy() {
        initCache();
        System.out.println("Cache created! Size: " + vocabulary.size());
    }

    @Override
    public void store() {
        System.out.println(statistics.getResult());
        storeCache();
    }

    private void initCache() {
        FileInputStream fis = null;
        try {
            String vacabularyTempFile = System.getProperty("java.io.tmpdir") + "vocabulary.ser";
            System.out.println("Vocabulary is cached to [ " + vacabularyTempFile +  " ]");
            fis = new FileInputStream(vacabularyTempFile);
            ObjectInputStream ois = new ObjectInputStream(fis);
            vocabulary = (Map<String, String>) ois.readObject();
            untranslated = (Map<String, String>) ois.readObject();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GTranslateProxy.class.getName()).log(Level.WARNING, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GTranslateProxy.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(GTranslateProxy.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(GTranslateProxy.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void storeCache() {
        FileOutputStream fos = null;
        try {
            String vacabularyTempFile = System.getProperty("java.io.tmpdir") + "vocabulary.ser";
            fos = new FileOutputStream(vacabularyTempFile);
//            fos = new FileOutputStream("/home/pavel/NetBeansProjects/gt-parse-output/src/main/resources/vocabulary.ser");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(vocabulary);
            oos.writeObject(untranslated);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GTranslateProxy.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GTranslateProxy.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(GTranslateProxy.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public String translate(String sourceText, String sourceLang, String targetLang) throws TranslationNotFoundException {
        if (untranslated.containsKey(sourceText)) {
            statistics.untranslatedUsed();
            return untranslated.get(sourceText);
        } else if (vocabulary.containsKey(sourceText)) {
            statistics.vocabularyUsed();
            return vocabulary.get(sourceText);
        } else {
            String translatedText = impl.translate(sourceText, sourceLang, targetLang);
            vocabulary.put(sourceText, translatedText);
            statistics.gapiUsed();
            return translatedText;
        }
    }

}
