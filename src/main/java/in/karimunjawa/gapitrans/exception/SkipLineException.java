/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.karimunjawa.gapitrans.exception;

/**
 *
 * @author pavel
 */
public class SkipLineException extends Exception {

    public SkipLineException(String string) {
        super(string);
    }

    public SkipLineException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public SkipLineException() {
        super();
    }
    
}
