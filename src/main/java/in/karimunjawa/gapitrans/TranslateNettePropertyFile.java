/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.karimunjawa.gapitrans;

import in.karimunjawa.gapitrans.exception.SkipLineException;
import in.karimunjawa.gapitrans.exception.TranslationNotFoundException;
import in.karimunjawa.gapitrans.util.StringUtil;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pavel
 */
public class TranslateNettePropertyFile {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        BufferedReader br = null;
        FileOutputStream fos = null;
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(args[0]), "utf-8"));
            fos = new FileOutputStream(args[0] + ".translated");
            String line;
            GTranslate gTranslate = GTranslateFactory.createInstance();
            while((line = br.readLine()) != null) {
                try {
                    String forTranslation = getValue(line);
                    String translated = gTranslate.translate(forTranslation, "en", "cs");
                    line = "\t" + getKey(line) + ": \"" + translated + "\"";
                } catch (SkipLineException ex) {
                    // line with module -> keep riginal
                } catch (TranslationNotFoundException ex) {
                    // translation not found
                    System.out.println("WARNING: translation not found " + line);
                }
                line += "\n";
                fos.write(line.getBytes("UTF-8"));
            }
            // print statistics and store cache
            GTranslateFactory.disposeInstance(gTranslate);
        } catch (IOException ex) {
            Logger.getLogger(TranslateNettePropertyFile.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (br != null) br.close();
            } catch (IOException ex) {
            }
            try {
                if (fos != null) fos.close();
            } catch (IOException ex) {
            }
        }
    }

    protected static String getKey(String line) {
        return line.substring(0, line.indexOf(":")).trim();
    }

    protected static String getValue(String line) throws SkipLineException {
        if (line.matches("^\\s*.*:.{1,}$")) {
            String value = line.substring(line.indexOf(":")+1).trim();
            return StringUtil.unwrap(value);
        } else {
            throw new SkipLineException();
        }
    }

}
