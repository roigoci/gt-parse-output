/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.karimunjawa.gapitrans;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pavel
 */
public class PostProcessPropertyFile {
    
    private static final String propertyFile = "/opt/lampp/htdocs/web/newllumarcz/app/lang/messages.cs_CZ.neon";
    
    public static void main(String[] args) {
        BufferedReader file = null;
        try {
            file = new BufferedReader(new FileReader(propertyFile));
            String line;
            StringBuffer inputBuffer = new StringBuffer();
            while ((line = file.readLine()) != null) {
                inputBuffer.append(line);
                inputBuffer.append('\n');
            }
            String inputStr = inputBuffer.toString();
            file.close();
            
            inputStr = inputStr.replace("\\u0026 Nbsp; \\u0026 amp; nbsp;", " & ");
            inputStr = inputStr.replace("\\u0026 amp;", "&");
            inputStr = inputStr.replace("\\u0026 Amp;", "&");
            inputStr = inputStr.replace("\\u0026 nbsp;", " ");
            inputStr = inputStr.replace("\\u0026 Nbsp;", " ");
            inputStr = inputStr.replace("\\u0026 lt;", "<");
            inputStr = inputStr.replace("\\u0026 gt;", ">");
            inputStr = inputStr.replace("\\u0026 ", "&"); // consider to use 'a' for czech

            // write the new String with the replaced line OVER the same file
            FileOutputStream fileOut = new FileOutputStream(propertyFile);
            fileOut.write(inputStr.getBytes("UTF-8"));
            fileOut.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PostProcessPropertyFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PostProcessPropertyFile.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (file != null) {
                    file.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(PostProcessPropertyFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("Done");
    }
    
}
