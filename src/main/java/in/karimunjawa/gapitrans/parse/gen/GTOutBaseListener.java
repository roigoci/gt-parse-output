package in.karimunjawa.gapitrans.parse.gen;

// Generated from GTOut.g4 by ANTLR 4.0

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.antlr.v4.runtime.tree.ErrorNode;

public class GTOutBaseListener implements GTOutListener {
	@Override public void enterTranslations(GTOutParser.TranslationsContext ctx) { }
	@Override public void exitTranslations(GTOutParser.TranslationsContext ctx) { }

	@Override public void enterSource_language(GTOutParser.Source_languageContext ctx) { }
	@Override public void exitSource_language(GTOutParser.Source_languageContext ctx) { }

	@Override public void enterDestination(GTOutParser.DestinationContext ctx) { }
	@Override public void exitDestination(GTOutParser.DestinationContext ctx) { }

	@Override public void enterDestination_language(GTOutParser.Destination_languageContext ctx) { }
	@Override public void exitDestination_language(GTOutParser.Destination_languageContext ctx) { }

	@Override public void enterSource(GTOutParser.SourceContext ctx) { }
	@Override public void exitSource(GTOutParser.SourceContext ctx) { }

	@Override public void enterTranslate(GTOutParser.TranslateContext ctx) { }
	@Override public void exitTranslate(GTOutParser.TranslateContext ctx) { }

	@Override public void enterOut(GTOutParser.OutContext ctx) { }
	@Override public void exitOut(GTOutParser.OutContext ctx) { }

	@Override public void enterEveryRule(ParserRuleContext ctx) { }
	@Override public void exitEveryRule(ParserRuleContext ctx) { }
	@Override public void visitTerminal(TerminalNode node) { }
	@Override public void visitErrorNode(ErrorNode node) { }
}