package in.karimunjawa.gapitrans.parse.gen;

// Generated from GTOut.g4 by ANTLR 4.0
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class GTOutLexer extends Lexer {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__4=1, T__3=2, T__2=3, T__1=4, T__0=5, NUMBER=6, LANGUAGE_CODE=7, StringLiteral=8;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] tokenNames = {
		"<INVALID>",
		"',null,'", "',null,null,'", "'['", "','", "']'", "NUMBER", "LANGUAGE_CODE", 
		"StringLiteral"
	};
	public static final String[] ruleNames = {
		"T__4", "T__3", "T__2", "T__1", "T__0", "NUMBER", "LANGUAGE_CODE", "StringLiteral", 
		"StringCharacters", "StringCharacter", "EscapeSequence", "UnicodeEscape", 
		"HexDigit"
	};


	public GTOutLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "GTOut.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\2\4\n_\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4"+
		"\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\3\2\3\2\3\2\3\2\3\2"+
		"\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\5\3"+
		"\5\3\6\3\6\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\5\bA\n\b\3\t\3\t\5"+
		"\tE\n\t\3\t\3\t\3\n\6\nJ\n\n\r\n\16\nK\3\13\3\13\5\13P\n\13\3\f\3\f\3"+
		"\f\5\fU\n\f\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\16\3\16\2\17\3\3\1\5\4\1\7\5"+
		"\1\t\6\1\13\7\1\r\b\1\17\t\1\21\n\1\23\2\1\25\2\1\27\2\1\31\2\1\33\2\1"+
		"\3\2\5\4$$^^\4$$^^\5\62;CHch^\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t"+
		"\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\3\35\3\2\2"+
		"\2\5$\3\2\2\2\7\60\3\2\2\2\t\62\3\2\2\2\13\64\3\2\2\2\r\66\3\2\2\2\17"+
		"@\3\2\2\2\21B\3\2\2\2\23I\3\2\2\2\25O\3\2\2\2\27T\3\2\2\2\31V\3\2\2\2"+
		"\33]\3\2\2\2\35\36\7.\2\2\36\37\7p\2\2\37 \7w\2\2 !\7n\2\2!\"\7n\2\2\""+
		"#\7.\2\2#\4\3\2\2\2$%\7.\2\2%&\7p\2\2&\'\7w\2\2\'(\7n\2\2()\7n\2\2)*\7"+
		".\2\2*+\7p\2\2+,\7w\2\2,-\7n\2\2-.\7n\2\2./\7.\2\2/\6\3\2\2\2\60\61\7"+
		"]\2\2\61\b\3\2\2\2\62\63\7.\2\2\63\n\3\2\2\2\64\65\7_\2\2\65\f\3\2\2\2"+
		"\66\67\4\62;\2\67\16\3\2\2\289\7$\2\29:\7e\2\2:;\7u\2\2;A\7$\2\2<=\7$"+
		"\2\2=>\7g\2\2>?\7p\2\2?A\7$\2\2@8\3\2\2\2@<\3\2\2\2A\20\3\2\2\2BD\7$\2"+
		"\2CE\5\23\n\2DC\3\2\2\2DE\3\2\2\2EF\3\2\2\2FG\7$\2\2G\22\3\2\2\2HJ\5\25"+
		"\13\2IH\3\2\2\2JK\3\2\2\2KI\3\2\2\2KL\3\2\2\2L\24\3\2\2\2MP\n\2\2\2NP"+
		"\5\27\f\2OM\3\2\2\2ON\3\2\2\2P\26\3\2\2\2QR\7^\2\2RU\t\3\2\2SU\5\31\r"+
		"\2TQ\3\2\2\2TS\3\2\2\2U\30\3\2\2\2VW\7^\2\2WX\7w\2\2XY\5\33\16\2YZ\5\33"+
		"\16\2Z[\5\33\16\2[\\\5\33\16\2\\\32\3\2\2\2]^\t\4\2\2^\34\3\2\2\2\b\2"+
		"@DKOT";
	public static final ATN _ATN =
		ATNSimulator.deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
	}
}