package in.karimunjawa.gapitrans.parse.gen;

// Generated from GTOut.g4 by ANTLR 4.0
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.Token;

public interface GTOutListener extends ParseTreeListener {
	void enterTranslations(GTOutParser.TranslationsContext ctx);
	void exitTranslations(GTOutParser.TranslationsContext ctx);

	void enterSource_language(GTOutParser.Source_languageContext ctx);
	void exitSource_language(GTOutParser.Source_languageContext ctx);

	void enterDestination(GTOutParser.DestinationContext ctx);
	void exitDestination(GTOutParser.DestinationContext ctx);

	void enterDestination_language(GTOutParser.Destination_languageContext ctx);
	void exitDestination_language(GTOutParser.Destination_languageContext ctx);

	void enterSource(GTOutParser.SourceContext ctx);
	void exitSource(GTOutParser.SourceContext ctx);

	void enterTranslate(GTOutParser.TranslateContext ctx);
	void exitTranslate(GTOutParser.TranslateContext ctx);

	void enterOut(GTOutParser.OutContext ctx);
	void exitOut(GTOutParser.OutContext ctx);
}