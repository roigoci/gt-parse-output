package in.karimunjawa.gapitrans.parse.gen;

// Generated from GTOut.g4 by ANTLR 4.0
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class GTOutParser extends Parser {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__4=1, T__3=2, T__2=3, T__1=4, T__0=5, NUMBER=6, LANGUAGE_CODE=7, StringLiteral=8;
	public static final String[] tokenNames = {
		"<INVALID>", "',null,'", "',null,null,'", "'['", "','", "']'", "NUMBER", 
		"LANGUAGE_CODE", "StringLiteral"
	};
	public static final int
		RULE_out = 0, RULE_translations = 1, RULE_translate = 2, RULE_destination_language = 3, 
		RULE_source_language = 4, RULE_destination = 5, RULE_source = 6;
	public static final String[] ruleNames = {
		"out", "translations", "translate", "destination_language", "source_language", 
		"destination", "source"
	};

	@Override
	public String getGrammarFileName() { return "GTOut.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public GTOutParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class OutContext extends ParserRuleContext {
		public TranslationsContext translations() {
			return getRuleContext(TranslationsContext.class,0);
		}
		public Source_languageContext source_language() {
			return getRuleContext(Source_languageContext.class,0);
		}
		public OutContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_out; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GTOutListener ) ((GTOutListener)listener).enterOut(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GTOutListener ) ((GTOutListener)listener).exitOut(this);
		}
	}

	public final OutContext out() throws RecognitionException {
		OutContext _localctx = new OutContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_out);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(14); match(3);
			setState(15); translations();
			setState(16); match(1);
			setState(17); source_language();
			setState(18); match(5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TranslationsContext extends ParserRuleContext {
		public List<TranslateContext> translate() {
			return getRuleContexts(TranslateContext.class);
		}
		public TranslateContext translate(int i) {
			return getRuleContext(TranslateContext.class,i);
		}
		public TranslationsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_translations; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GTOutListener ) ((GTOutListener)listener).enterTranslations(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GTOutListener ) ((GTOutListener)listener).exitTranslations(this);
		}
	}

	public final TranslationsContext translations() throws RecognitionException {
		TranslationsContext _localctx = new TranslationsContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_translations);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(20); match(3);
			setState(21); translate();
			setState(26);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==4) {
				{
				{
				setState(22); match(4);
				setState(23); translate();
				}
				}
				setState(28);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(29); match(5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TranslateContext extends ParserRuleContext {
		public TerminalNode NUMBER() { return getToken(GTOutParser.NUMBER, 0); }
		public DestinationContext destination() {
			return getRuleContext(DestinationContext.class,0);
		}
		public SourceContext source() {
			return getRuleContext(SourceContext.class,0);
		}
		public TranslateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_translate; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GTOutListener ) ((GTOutListener)listener).enterTranslate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GTOutListener ) ((GTOutListener)listener).exitTranslate(this);
		}
	}

	public final TranslateContext translate() throws RecognitionException {
		TranslateContext _localctx = new TranslateContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_translate);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(31); match(3);
			setState(32); destination();
			setState(33); match(4);
			setState(34); source();
			setState(35); match(2);
			setState(36); match(NUMBER);
			setState(37); match(5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Destination_languageContext extends ParserRuleContext {
		public TerminalNode LANGUAGE_CODE() { return getToken(GTOutParser.LANGUAGE_CODE, 0); }
		public Destination_languageContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_destination_language; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GTOutListener ) ((GTOutListener)listener).enterDestination_language(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GTOutListener ) ((GTOutListener)listener).exitDestination_language(this);
		}
	}

	public final Destination_languageContext destination_language() throws RecognitionException {
		Destination_languageContext _localctx = new Destination_languageContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_destination_language);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(39); match(LANGUAGE_CODE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Source_languageContext extends ParserRuleContext {
		public TerminalNode LANGUAGE_CODE() { return getToken(GTOutParser.LANGUAGE_CODE, 0); }
		public Source_languageContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_source_language; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GTOutListener ) ((GTOutListener)listener).enterSource_language(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GTOutListener ) ((GTOutListener)listener).exitSource_language(this);
		}
	}

	public final Source_languageContext source_language() throws RecognitionException {
		Source_languageContext _localctx = new Source_languageContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_source_language);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(41); match(LANGUAGE_CODE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DestinationContext extends ParserRuleContext {
		public TerminalNode StringLiteral() { return getToken(GTOutParser.StringLiteral, 0); }
		public DestinationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_destination; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GTOutListener ) ((GTOutListener)listener).enterDestination(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GTOutListener ) ((GTOutListener)listener).exitDestination(this);
		}
	}

	public final DestinationContext destination() throws RecognitionException {
		DestinationContext _localctx = new DestinationContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_destination);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(43); match(StringLiteral);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SourceContext extends ParserRuleContext {
		public TerminalNode StringLiteral() { return getToken(GTOutParser.StringLiteral, 0); }
		public SourceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_source; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GTOutListener ) ((GTOutListener)listener).enterSource(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GTOutListener ) ((GTOutListener)listener).exitSource(this);
		}
	}

	public final SourceContext source() throws RecognitionException {
		SourceContext _localctx = new SourceContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_source);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(45); match(StringLiteral);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\2\3\n\62\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\3\2"+
		"\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\7\3\33\n\3\f\3\16\3\36\13\3\3\3\3"+
		"\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\b"+
		"\2\t\2\4\6\b\n\f\16\2\2+\2\20\3\2\2\2\4\26\3\2\2\2\6!\3\2\2\2\b)\3\2\2"+
		"\2\n+\3\2\2\2\f-\3\2\2\2\16/\3\2\2\2\20\21\7\5\2\2\21\22\5\4\3\2\22\23"+
		"\7\3\2\2\23\24\5\n\6\2\24\25\7\7\2\2\25\3\3\2\2\2\26\27\7\5\2\2\27\34"+
		"\5\6\4\2\30\31\7\6\2\2\31\33\5\6\4\2\32\30\3\2\2\2\33\36\3\2\2\2\34\32"+
		"\3\2\2\2\34\35\3\2\2\2\35\37\3\2\2\2\36\34\3\2\2\2\37 \7\7\2\2 \5\3\2"+
		"\2\2!\"\7\5\2\2\"#\5\f\7\2#$\7\6\2\2$%\5\16\b\2%&\7\4\2\2&\'\7\b\2\2\'"+
		"(\7\7\2\2(\7\3\2\2\2)*\7\t\2\2*\t\3\2\2\2+,\7\t\2\2,\13\3\2\2\2-.\7\n"+
		"\2\2.\r\3\2\2\2/\60\7\n\2\2\60\17\3\2\2\2\3\34";
	public static final ATN _ATN =
		ATNSimulator.deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
	}
}