package in.karimunjawa.gapitrans.parse;

import in.karimunjawa.gapitrans.exception.TranslationNotFoundException;
import in.karimunjawa.gapitrans.parse.gen.GTOutParser;
import in.karimunjawa.gapitrans.parse.gen.GTOutLexer;
import in.karimunjawa.gapitrans.parse.gen.GTOutBaseListener;
import in.karimunjawa.gapitrans.util.StringUtil;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pavel
 */
public class GoogleTranslateOutputParser extends GTOutBaseListener {

    public class Output {
        private String sourceText;
        private String translatedText;

        public Output(String sourceText, String translatedText) {
            this.sourceText = sourceText;
            this.translatedText = translatedText;
        }

        public String getSourceText() {
            return sourceText;
        }

        public String getTranslatedText() {
            return translatedText;
        }
        
    }
    
    private StringBuffer translatedText;
    private StringBuffer sourceText;
    private String destination;
    private StringBuffer translationError;
    
    public GoogleTranslateOutputParser() {
        translatedText = new StringBuffer();
        sourceText = new StringBuffer();
        translationError = new StringBuffer();
    }

    protected Output getParcedData() throws TranslationNotFoundException {
        checkError();
        return new Output(sourceText.toString().trim(), translatedText.toString());
    }
    
    public static Output parse(String output) throws TranslationNotFoundException {
        ANTLRInputStream input = new ANTLRInputStream(output);
        
        GTOutLexer lexer = new GTOutLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        GTOutParser parser = new GTOutParser(tokens);
        ParseTree tree = parser.out();
        
        GoogleTranslateOutputParser listener = new GoogleTranslateOutputParser();
        
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(listener, tree);

        return listener.getParcedData();
    }
    
    protected void checkError() throws TranslationNotFoundException {
        String errors = translationError.toString();
        if (!errors.isEmpty()) {
            throw new TranslationNotFoundException(errors);
        }
    }

    @Override
    public void enterDestination(GTOutParser.DestinationContext ctx) {
        destination = StringUtil.unwrap(ctx.getText());
        translatedText.append(destination);
    }

    @Override
    public void enterSource(GTOutParser.SourceContext ctx) {
        String source = StringUtil.unwrap(ctx.getText());
        sourceText.append(source).append(" ");
        if (destination == null || destination.trim().equals(source)) {
            translationError.append(sourceText);
        }
    }
    
}