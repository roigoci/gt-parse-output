/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.karimunjawa.gapitrans;

import in.karimunjawa.gapitrans.exception.TranslationNotFoundException;
import in.karimunjawa.gapitrans.parse.GoogleTranslateOutputParser;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;

/**
 *
 * @author pavel
 */
public class GTranslateImpl implements GTranslate {

    /**
     * 
     * 
     * @param sourceText
     * @param sourceLang
     * @param targetLang
     * @return
     * @throws TranslationNotFoundException 
     */
    public String translate(String sourceText, String sourceLang, String targetLang) throws TranslationNotFoundException {
        String encoding = "UTF-8";
        try {
            String url = "https://translate.googleapis.com/translate_a/single?client=gtx&sl="
                + sourceLang + "&tl=" + targetLang + "&dt=t&q=" + URLEncoder.encode(sourceText, encoding);
            String output = getUrl(url);
            System.out.println("Output: " + output);
            return parseGTOutput(output);
        } catch (UnsupportedEncodingException ex) {
            throw new TranslationNotFoundException("Unsupported encodind [ " + encoding + " ] ", ex);
        }
    }
    
    /**
     * Parse json like output from google translate service
     * 
     * @param json
     * @return 
     */
    protected static String parseGTOutput(String json) throws TranslationNotFoundException {
        return GoogleTranslateOutputParser.parse(json).getTranslatedText();
    }
    
    protected static String getUrl(String url) {
        // create http client
        HttpClient client = new HttpClient();
        client.getHttpConnectionManager().getParams().setConnectionTimeout(5000);
        
        // prepare call method
        HttpMethod method = new GetMethod(url);
        method.setFollowRedirects(true);
        method.setRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36");
        
        String responseBody = "";
        try {
            // call url
            client.executeMethod(method);
            // get output
            responseBody = method.getResponseBodyAsString();
        } catch (IOException ex) {
            Logger.getLogger(GTranslateImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return responseBody;
    }
    
}
