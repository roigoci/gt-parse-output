/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.karimunjawa.gapitrans;

import in.karimunjawa.gapitrans.exception.TranslationNotFoundException;

/**
 *
 * @author pavel
 */
public interface GTranslate {

    public String translate(String sourceText, String sourceLang, String targetLang) throws TranslationNotFoundException;

}
